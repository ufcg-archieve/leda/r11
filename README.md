# Roteiro 11
### Árvore Binária de Busca

- [BST](https://gitlab.com/ufcg-archieve/leda/r11/-/blob/968deeecbaddb27dd551f77360f3d5b8cf83d83c/src/main/java/adt/bst/BSTImpl.java)
- [SimpleBSTManipulation](https://gitlab.com/ufcg-archieve/leda/r11/-/blob/968deeecbaddb27dd551f77360f3d5b8cf83d83c/src/main/java/adt/bst/SimpleBSTManipulationImpl.java)
- [FloorCeilBST](https://gitlab.com/ufcg-archieve/leda/r11/-/blob/968deeecbaddb27dd551f77360f3d5b8cf83d83c/src/main/java/adt/bst/extended/FloorCeilBSTImpl.java)
