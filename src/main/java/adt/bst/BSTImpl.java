package adt.bst;

import java.util.ArrayList;
import java.util.List;

public class BSTImpl<T extends Comparable<T>> implements BST<T> {

	protected BSTNode<T> root;

	public BSTImpl() {
		root = new BSTNode<T>();
	}

	public BSTNode<T> getRoot() {
		return this.root;
	}

	@Override
	public boolean isEmpty() {
		return root.isEmpty();
	}

	@Override
	public int height() {
		return height(this.root);
	}

	private int height(BSTNode<T> node) {
		int height = -1;
		if (!node.isEmpty()) {
			height = 1 + Math.max(height((BSTNode<T>) node.getLeft()), height((BSTNode<T>) node.getRight()));
		}
		return height;
	}

	@Override
	public BSTNode<T> search(T element) {
		return search(element, this.root);
	}

	private BSTNode<T> search(T element, BSTNode<T> node) {
		BSTNode<T> result = new BSTNode<>();
		if (element != null && !node.isEmpty()) {
			if (element.equals(node.getData())) {
				result = node;
			} else if (element.compareTo(node.getData()) < 0) {
				result = search(element, (BSTNode<T>) node.getLeft());
			} else {
				result = search(element, (BSTNode<T>) node.getRight());
			}
		}
		return result;
	}

	@Override
	public void insert(T element) {
		if (element != null) {
			insert(element, this.root);
		}
	}

	private void insert(T element, BSTNode<T> node) {
		if (node.isEmpty()) {
			node.setData(element);
			node.setLeft(new BSTNode<>());
			node.setRight(new BSTNode<>());
			node.getLeft().setParent(node);
			node.getRight().setParent(node);
		} else {
			if (element.compareTo(node.getData()) < 0) {
				insert(element, (BSTNode<T>) node.getLeft());
			} else {
				insert(element, (BSTNode<T>) node.getRight());
			}
		}
	}

	@Override
	public BSTNode<T> maximum() {
		BSTNode<T> max = null;
		if (!isEmpty()) {
			max = maximum(this.root);
		}
		return max;
	}

	private BSTNode<T> maximum(BSTNode<T> node) {
		BSTNode<T> max = node;
		if (!node.getRight().isEmpty()) {
			max = maximum((BSTNode<T>) node.getRight());
		}
		return max;
	}

	@Override
	public BSTNode<T> minimum() {
		BSTNode<T> min = null;
		if (!isEmpty()) {
			min = minimum(this.root);
		}
		return min;
	}

	private BSTNode<T> minimum(BSTNode<T> node) {
		BSTNode<T> min = node;
		if (!node.getLeft().isEmpty()) {
			min = minimum((BSTNode<T>) node.getLeft());
		}
		return min;
	}

	@Override
	public BSTNode<T> sucessor(T element) {
		BSTNode<T> node = search(element);
		BSTNode<T> sucessor = null;
		if (!node.isEmpty()) {
			if (!node.getRight().isEmpty()) {
				sucessor = minimum((BSTNode<T>) node.getRight());
			} else {
				sucessor = (BSTNode<T>) node.getParent();
				while (sucessor != null && sucessor.getData().compareTo(element) < 0) {
					sucessor = (BSTNode<T>) sucessor.getParent();
				}
			}
		}
		return sucessor;
	}

	@Override
	public BSTNode<T> predecessor(T element) {
		BSTNode<T> node = search(element);
		BSTNode<T> predecessor = null;
		if (!node.isEmpty()) {
			if (!node.getLeft().isEmpty()) {
				predecessor = maximum((BSTNode<T>) node.getLeft());
			} else {
				predecessor = (BSTNode<T>) node.getParent();
				while (predecessor != null && predecessor.getData().compareTo(element) > 0) {
					predecessor = (BSTNode<T>) predecessor.getParent();
				}
			}
		}
		return predecessor;
	}

	@Override
	public void remove(T element) {
		BSTNode<T> node = search(element);
		if (!node.isEmpty()) {

			// Nó folha (sem filhos)
			if (node.isLeaf()) {
				node.setData(null);
			}

			// Nó possui 1 filho
			else if (hasOnlyChild(node)) {
				BSTNode<T> child = node.getLeft().isEmpty() ? (BSTNode<T>) node.getRight() : (BSTNode<T>) node.getLeft();
				if (this.root.equals(node)) {
					this.root = child;
					this.root.setParent(null);
				} else {
					BSTNode<T> parent = (BSTNode<T>) node.getParent();
					child.setParent(parent);
					if (parent.getLeft().equals(node)) {
						parent.setLeft(child);
					} else {
						parent.setRight(child);
					}
				}
			}

			// Nó possui 2 filhos
			else {
				T sucessor = sucessor(node.getData()).getData();
				remove(sucessor);
				node.setData(sucessor);
			}
		}
	}

	private boolean hasOnlyChild(BSTNode<T> node) {
		return (node.getLeft().isEmpty() && !node.getRight().isEmpty())
			|| (!node.getLeft().isEmpty() && node.getRight().isEmpty());
	}

	@Override
	public T[] preOrder() {
		List<T> list = new ArrayList<>();
		preOrder(list, this.root);
		return (T[]) list.toArray(new Comparable[list.size()]);
	}

	private void preOrder(List<T> list, BSTNode<T> node) {
		if (!node.isEmpty()) {
			list.add(node.getData());
			preOrder(list, (BSTNode<T>) node.getLeft());
			preOrder(list, (BSTNode<T>) node.getRight());
		}
	}

	@Override
	public T[] order() {
		List<T> list = new ArrayList<>();
		order(list, this.root);
		return (T[]) list.toArray(new Comparable[list.size()]);
	}

	private void order(List<T> list, BSTNode<T> node) {
		if (!node.isEmpty()) {
			order(list, (BSTNode<T>) node.getLeft());
			list.add(node.getData());
			order(list, (BSTNode<T>) node.getRight());
		}
	}

	@Override
	public T[] postOrder() {
		List<T> list = new ArrayList<>();
		postOrder(list, this.root);
		return (T[]) list.toArray(new Comparable[list.size()]);
	}

	private void postOrder(List<T> list, BSTNode<T> node) {
		if (!node.isEmpty()) {
			postOrder(list, (BSTNode<T>) node.getLeft());
			postOrder(list, (BSTNode<T>) node.getRight());
			list.add(node.getData());
		}
	}

	/**
	 * This method is already implemented using recursion. You must understand
	 * how it work and use similar idea with the other methods.
	 */
	@Override
	public int size() {
		return size(root);
	}

	private int size(BSTNode<T> node) {
		int result = 0;
		// base case means doing nothing (return 0)
		if (!node.isEmpty()) { // inductive case
			result = 1 + size((BSTNode<T>) node.getLeft())
					+ size((BSTNode<T>) node.getRight());
		}
		return result;
	}

}