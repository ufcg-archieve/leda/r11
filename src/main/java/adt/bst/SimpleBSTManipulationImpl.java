package adt.bst;

/**
 * - Esta eh a unica classe que pode ser modificada 
 * @author adalbertocajueiro
 *
 * @param <T>
 */
public class SimpleBSTManipulationImpl<T extends Comparable<T>> implements SimpleBSTManipulation<T> {

	@Override
	public boolean equals(BST<T> tree1, BST<T> tree2) {
		boolean equals = false;
		if (tree1 != null && tree2 != null) {
			equals = equals((BSTNode<T>) tree1.getRoot(), (BSTNode<T>) tree2.getRoot());
		}
		return equals;
	}

	private boolean equals(BSTNode<T> node1, BSTNode<T> node2) {
		boolean equals = false;
		if (node1.isEmpty() && node2.isEmpty()) {
			equals = true;
		} else if (node1.equals(node2)) {
			equals = equals((BSTNode<T>) node1.getLeft(), (BSTNode<T>) node2.getLeft())
									&& equals((BSTNode<T>) node1.getRight(), (BSTNode<T>) node2.getRight());
		}
		return equals;
	}

	@Override
	public boolean isSimilar(BST<T> tree1, BST<T> tree2) {
		boolean isSimilar = false;
		if (tree1 != null && tree2 != null) {
			isSimilar = isSimilar((BSTNode<T>) tree1.getRoot(), (BSTNode<T>) tree2.getRoot());
		}
		return isSimilar;
	}

	private boolean isSimilar(BSTNode<T> node1, BSTNode<T> node2) {
		boolean isSimilar = false;
		if (node1.isEmpty() && node2.isEmpty()) {
			isSimilar = true;
		} else if (!node1.isEmpty() && !node2.isEmpty()) {
			isSimilar = isSimilar((BSTNode<T>) node1.getLeft(), (BSTNode<T>) node2.getLeft())
										&& isSimilar((BSTNode<T>) node1.getRight(), (BSTNode<T>) node2.getRight());
		}
		return isSimilar;
	}

	@Override
	public T orderStatistic(BST<T> tree, int k) {
		T element = null;
		if (tree != null && k > 0 && k <= tree.size()) {
			element = orderStatistic((BSTNode<T>) tree.getRoot(), k);
		}
		return element;
	}

	private T orderStatistic(BSTNode<T> node, int k) {
		T element = null;
		if (k > 0) {
			int orderStatistic = size((BSTNode<T>) node.getLeft()) + 1;
			if (!node.isEmpty() && orderStatistic == k) {
				element = node.getData();
			} else if (k < orderStatistic) {
				element = orderStatistic((BSTNode<T>) node.getLeft(), k);
			} else {
				element = orderStatistic((BSTNode<T>) node.getRight(), k - orderStatistic);
			}
		}
		return element;
	}

	private int size(BSTNode<T> node) {
		int size = 0;
		if (!node.isEmpty()) {
			size = 1 + size((BSTNode<T>) node.getLeft()) + size((BSTNode<T>) node.getRight());
		}
		return size;
	}

}