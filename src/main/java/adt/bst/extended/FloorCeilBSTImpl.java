package adt.bst.extended;

import adt.bst.BSTImpl;
import adt.bst.BSTNode;

/**
 * Note que esta classe estende sua implementacao de BST (BSTImpl).
 * Dependendo do design que voce use, sua BSTImpl precisa ter apenas funcionando
 * corretamente o metodo insert para que voce consiga testar esta classe.
 */
public class FloorCeilBSTImpl extends BSTImpl<Integer> implements FloorCeilBST {

	@Override
	public Integer floor(Integer[] array, double numero) {
		Integer floor = null;
		if (array != null && array.length > 0) {
			for (Integer valor : array) {
				this.insert(valor);
			}
			floor = floor(this.root, numero);
		}
		return floor;
	}

	private Integer floor(BSTNode<Integer> node, double numero) {
		Integer floor = null;
		if (!node.isEmpty()) {
			if (node.getData() == numero) {
				floor = node.getData();
			} else if (node.getData() < numero) {
				floor = floor((BSTNode<Integer>) node.getRight(), numero);
				if (floor == null) {
					floor = node.getData();
				}
			} else {
				floor = floor((BSTNode<Integer>) node.getLeft(), numero);
			}
		}
		return floor;
	}

	@Override
	public Integer ceil(Integer[] array, double numero) {
		Integer ceil = null;
		if (array != null && array.length > 0) {
			for (Integer valor : array) {
				this.insert(valor);
			}
			ceil = ceil(this.root, numero);
		}
		return ceil;
	}

	private Integer ceil(BSTNode<Integer> node, double numero) {
		Integer ceil = null;
		if (!node.isEmpty()) {
			if (node.getData() == numero) {
				ceil = node.getData();
			} else if (node.getData() > numero) {
				ceil = ceil((BSTNode<Integer>) node.getLeft(), numero);
				if (ceil == null) {
					ceil = node.getData();
				}
			} else {
				ceil = ceil((BSTNode<Integer>) node.getRight(), numero);
			}
		}
		return ceil;
	}

}