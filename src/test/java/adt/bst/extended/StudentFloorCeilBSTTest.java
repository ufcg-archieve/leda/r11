package adt.bst.extended;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

public class StudentFloorCeilBSTTest {
    private FloorCeilBST impl;
    private Integer[] array;
    
    @Before
    public void setUp() {
        impl = new FloorCeilBSTImpl();
        array = new Integer[]{ 6, 23, -34, 5, 9, 2, 0, 76, 12, 67, 232, -40};
    }

    @Test
    public void testFloor() {
        assertEquals(-40, impl.floor(array, -40), 0);
    }

    @Test
    public void testNoFloor() {
        assertNull(impl.floor(array, -41));
    }

    @Test
    public void testFloorElementOutOfTree() {
        assertEquals(0, impl.floor(array, 1), 0);
    }

    @Test
    public void testFloorNull() {
        assertNull(impl.floor(null, 0));
    }

    @Test
    public void testCeil() {
        assertEquals(-40, impl.ceil(array, -40), 0);
    }

    @Test
    public void testNoCeil() {
        assertNull(impl.ceil(array, 233));
    }

    @Test
    public void testCeilElementOutOfTree() {
        assertEquals(-40, impl.ceil(array, -45), 0);
    }

    @Test
    public void testCeilNull() {
        assertNull(impl.ceil(null, -45));
    }
}